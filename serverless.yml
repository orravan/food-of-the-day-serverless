# Welcome to Serverless!
#
# This file is the main config file for your service.
# It's very minimal at this point and uses default values.
# You can always add more config options for more control.
# We've included some commented out config examples here.
# Just uncomment any of them to get that config option.
#
# For full config options, check the docs:
#    docs.serverless.com
#
# Happy Coding!

service: fod-rest-api

custom:
  secrets: ${file(secrets.json)}

# You can pin your service to only deploy with a specific Serverless version
# Check out our docs for more details
# frameworkVersion: "=X.X.X"

provider:
  name: aws
  runtime: nodejs6.10

  # you can overwrite defaults here
  stage: dev
  region: us-east-1

  # you can add statements to the Lambda function's IAM Role here
  #  iamRoleStatements:
  #    - Effect: "Allow"
  #      Action:
  #        - "s3:ListBucket"
  #      Resource: { "Fn::Join" : ["", ["arn:aws:s3:::", { "Ref" : "ServerlessDeploymentBucket" } ] ]  }
  #    - Effect: "Allow"
  #      Action:
  #        - "s3:PutObject"
  #      Resource:
  #        Fn::Join:
  #          - ""
  #          - - "arn:aws:s3:::"
  #            - "Ref" : "ServerlessDeploymentBucket"
  #            - "/*"

  # you can define service wide environment variables here
  environment:
    JWT_SECRET: ${self:custom.secrets.JWT_SECRET}
    STRIPE_KEY: ${self:custom.secrets.STRIPE_KEY}
    ROLLBAR_TOKEN: ${self:custom.secrets.ROLLBAR_TOKEN}
    ROLLBAR_ENVIRONMENT: ${self:custom.secrets.ROLLBAR_ENVIRONMENT}
    MJ_APIKEY_PUBLIC: ${self:custom.secrets.MJ_APIKEY_PUBLIC}
    MJ_APIKEY_PRIVATE: ${self:custom.secrets.MJ_APIKEY_PRIVATE}
    ONE_SIGNAL_APP_ID: ${self:custom.secrets.ONE_SIGNAL_APP_ID}
    ONE_SIGNAL_APP_AUTH_KEY: ${self:custom.secrets.ONE_SIGNAL_APP_AUTH_KEY}
    ONE_SIGNAL_USER_AUTH_KEY: ${self:custom.secrets.ONE_SIGNAL_USER_AUTH_KEY}

# you can add packaging information here
#package:
#  include:
#    - include-me.js
#    - include-me-dir/**
#  exclude:
#    - exclude-me.js
#    - exclude-me-dir/**

functions:
  verify-token:
    handler: auth/VerifyToken.auth

  register:
    handler: auth/AuthHandler.register
    events:
      - http:
          path: user/register
          method: post
          cors: true

  login:
    handler: auth/AuthHandler.login
    events:
      - http:
          path: user/login
          method: post
          cors: true

  me:
    handler: auth/AuthHandler.me
    events:
      - http:
          path: user/me
          method: get
          cors: true
          authorizer: verify-token
  updateUser:
    handler: auth/AuthHandler.update
    events:
      - http:
          path: user/update
          method: put
          cors: true
          authorizer: verify-token

  updateProduct:
    handler: product/ProductHandler.update
    events:
      - http:
          path: product/update
          method: post
          cors: true

  createCard:
    handler: card/CardHandler.create
    events:
      - http:
          path: card/create
          method: post
          cors: true
          authorizer: verify-token

  deleteCard:
    handler: card/CardHandler.delete
    events:
      - http:
          path: card/delete
          method: delete
          cors: true
          authorizer: verify-token

  createOrder:
    handler: order/OrderHandler.create
    events:
      - http:
          path: order/create
          method: post
          cors: true
          authorizer: verify-token

  listOrder:
    handler: order/OrderHandler.list
    events:
      - http:
          path: order/list
          method: get
          cors: true
          authorizer: verify-token

  retrieveCoupon:
    handler: coupon/CouponHandler.retrieve
    events:
      - http:
          path: coupon
          method: post
          cors: true
          authorizer: verify-token

  updateOrder:
    handler: order/OrderHandler.update
    events:
      - http:
          path: order/update
          method: post
          cors: true

  resetPassword:
    handler: auth/AuthHandler.reset
    events:
      - http:
          path: user/reset
          method: post
          cors: true

#    The following are a few example events you can configure
#    NOTE: Please make sure to change your handler code to work with those events
#    Check the event documentation for details
#    events:
#      - http:
#          path: users/create
#          method: get
#      - s3: ${env:BUCKET}
#      - schedule: rate(10 minutes)
#      - sns: greeter-topic
#      - stream: arn:aws:dynamodb:region:XXXXXX:table/foo/stream/1970-01-01T00:00:00.000
#      - alexaSkill
#      - alexaSmartHome: amzn1.ask.skill.xx-xx-xx-xx
#      - iot:
#          sql: "SELECT * FROM 'some_topic'"
#      - cloudwatchEvent:
#          event:
#            source:
#              - "aws.ec2"
#            detail-type:
#              - "EC2 Instance State-change Notification"
#            detail:
#              state:
#                - pending
#      - cloudwatchLog: '/aws/lambda/hello'
#      - cognitoUserPool:
#          pool: MyUserPool
#          trigger: PreSignUp

#    Define function environment variables here
#    environment:
#      variable2: value2

# you can add CloudFormation resource templates here
#resources:
#  Resources:
#    NewResource:
#      Type: AWS::S3::Bucket
#      Properties:
#        BucketName: my-new-bucket
#  Outputs:
#     NewOutput:
#       Description: "Description for the output"
#       Value: "Some output value"

resources:
  Resources:
    GatewayResponseDefault4XX:
      Type: "AWS::ApiGateway::GatewayResponse"
      Properties:
        ResponseParameters:
          gatewayresponse.header.Access-Control-Allow-Origin: "'*'"
          gatewayresponse.header.Access-Control-Allow-Headers: "'*'"
        ResponseType: DEFAULT_4XX
        RestApiId:
          Ref: "ApiGatewayRestApi"

plugins:
  - serverless-offline # adding the plugin to be able to run the offline emulation
