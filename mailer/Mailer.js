const mailjet = require("node-mailjet").connect(
  process.env.MJ_APIKEY_PUBLIC,
  process.env.MJ_APIKEY_PRIVATE
);

/**
 * Email send for user registration
 */
module.exports.createAccount = (emailAddress, firstName, lastName) => {
  return mailjet.post("send", { version: "v3.1" }).request({
    Messages: [
      {
        From: {
          Email: "contact@food-of-the-day.fr",
          Name: "FOD"
        },
        To: [
          {
            Email: emailAddress,
            Name: firstName + " " + lastName
          }
        ],
        TemplateID: 718346,
        TemplateLanguage: true,
        Subject: "FOD - Confirmation d'inscription",
        Variables: {
          firstname: firstName,
          email: emailAddress
        }
      }
    ]
  });
};

/**
 * Function to format "amount" attribute for order items
 *
 * @param {*} orderItems
 */
function formatAmountItems(orderItems) {
  let orderItemsFormatted = [];

  orderItemsFormatted = orderItems.map(item => {
    item.amount = new Intl.NumberFormat("fr-FR", {
      style: "currency",
      currency: "EUR"
    }).format(item.amount / 100);

    return item;
  });

  return orderItemsFormatted;
}

/**
 * Email send for order creation
 */
module.exports.createOrder = (
  emailAddress,
  orderNumber,
  deliveryDate,
  deliveryPlace,
  orderItems,
  totalOrder
) => {
  return mailjet.post("send", { version: "v3.1" }).request({
    Messages: [
      {
        From: {
          Email: "contact@food-of-the-day.fr",
          Name: "FOD"
        },
        To: [
          {
            Email: emailAddress
          }
        ],
        TemplateID: 719996,
        TemplateLanguage: true,
        Subject: "FOD - Votre commande est validée",
        Variables: {
          orderNumber: orderNumber,
          deliveryDate: deliveryDate,
          deliveryPlace: deliveryPlace,
          orderItems: formatAmountItems(orderItems),
          totalOrder: new Intl.NumberFormat("fr-FR", {
            style: "currency",
            currency: "EUR"
          }).format(totalOrder / 100)
        }
      }
    ]
  });
};

/**
 * Email send for order shipped
 */
module.exports.orderShipped = (
  emailAddress,
  orderNumber,
  deliveryDate,
  deliveryPlace,
  orderItems,
  totalOrder
) => {
  return mailjet.post("send", { version: "v3.1" }).request({
    Messages: [
      {
        From: {
          Email: "contact@food-of-the-day.fr",
          Name: "FOD"
        },
        To: [
          {
            Email: emailAddress
          }
        ],
        TemplateID: 734705,
        TemplateLanguage: true,
        Subject: "FOD - Votre commande est livrée",
        Variables: {
          orderNumber: orderNumber,
          deliveryDate: deliveryDate,
          deliveryPlace: deliveryPlace,
          orderItems: formatAmountItems(orderItems),
          totalOrder: new Intl.NumberFormat("fr-FR", {
            style: "currency",
            currency: "EUR"
          }).format(totalOrder / 100)
        }
      }
    ]
  });
};

/**
 * Email send for order canceled
 */
module.exports.orderCanceled = (
  emailAddress,
  orderNumber,
  deliveryDate,
  deliveryPlace,
  orderItems,
  totalOrder
) => {
  return mailjet.post("send", { version: "v3.1" }).request({
    Messages: [
      {
        From: {
          Email: "contact@food-of-the-day.fr",
          Name: "FOD"
        },
        To: [
          {
            Email: emailAddress
          }
        ],
        TemplateID: 734767,
        TemplateLanguage: true,
        Subject: "FOD - Annulation de votre commande",
        Variables: {
          orderNumber: orderNumber,
          deliveryDate: deliveryDate,
          deliveryPlace: deliveryPlace,
          orderItems: formatAmountItems(orderItems),
          totalOrder: new Intl.NumberFormat("fr-FR", {
            style: "currency",
            currency: "EUR"
          }).format(totalOrder / 100)
        }
      }
    ]
  });
};

/**
 * Email send to reset password
 */
module.exports.resetPassword = (emailAddress, password) => {
  return mailjet.post("send", { version: "v3.1" }).request({
    Messages: [
      {
        From: {
          Email: "contact@food-of-the-day.fr",
          Name: "FOD"
        },
        To: [
          {
            Email: emailAddress
          }
        ],
        TemplateID: 741100,
        TemplateLanguage: true,
        Subject: "FOD - Réinitialisation de mot de passe",
        Variables: {
          password: password
        }
      }
    ]
  });
};
