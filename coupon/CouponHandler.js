const stripe = require("stripe")(process.env.STRIPE_KEY);
const Rollbar = require("rollbar");
const rollbar = new Rollbar({
  accessToken: process.env.ROLLBAR_TOKEN,
  environment: process.env.ROLLBAR_ENVIRONMENT,
  captureUncaught: true,
  captureUnhandledRejections: true
});

/**
 * Check if input is valid to retrieve a coupon
 *
 * @param {*} eventBody
 */
function checkIfInputIsValid(eventBody) {
  if (!eventBody.coupon) {
    return Promise.reject(new Error("Missing informations to get a coupon."));
  }

  return Promise.resolve();
}

/**
 * Route to retrieve a coupon
 *
 * @param {*} event
 * @param {*} context
 * @param {*} callback
 */
module.exports.retrieve = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  let eventBody = JSON.parse(event.body);

  return checkIfInputIsValid(eventBody) // validate input
    .then(coupon => stripe.coupons.retrieve(eventBody.coupon))
    .then(coupon =>
      callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Credentials": true
        },
        body: JSON.stringify({
          id: coupon.id,
          correct: coupon.valid,
          percent: coupon.percent_off,
          amount: coupon.amount_off
        })
      })
    )
    .catch(err => {
      console.error("COUPON - Retrieve failed", err, event.body);
      rollbar.error("COUPON - Retrieve failed", err, event.body);
      rollbar.wait(function() {
        return callback(null, {
          statusCode: err.statusCode || 500,
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
            "Content-Type": "text/plain"
          },
          body: JSON.stringify({ correct: false })
        });
      });
    });
};
