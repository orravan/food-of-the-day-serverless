const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs-then");
const stripe = require("stripe")(process.env.STRIPE_KEY);
const mailer = require("../mailer/Mailer");
const Rollbar = require("rollbar");
const rollbar = new Rollbar({
  accessToken: process.env.ROLLBAR_TOKEN,
  environment: process.env.ROLLBAR_ENVIRONMENT,
  captureUncaught: true,
  captureUnhandledRejections: true
});

/**
 * Error messsages
 */
const USER_ALREADY_EXISTS = "USER_ALREADY_EXISTS";
const WRONG_CREDENTIALS = "WRONG_CREDENTIALS";
const USER_DOESNT_EXIST = "USER_DOESNT_EXIST";
const PASSWORD_RESET = "PASSWORD_RESET";

/**
 * Generate a token for user session
 * @param {*} id
 */
function signToken(id) {
  return jwt.sign({ id: id }, process.env.JWT_SECRET, {
    expiresIn: 604800 // expires in 1 week
  });
}

/**
 * Check if input is valid for register and update user informations
 *
 * @param {*} eventBody
 */
function checkIfInputIsValid(eventBody, checkPassword = true) {
  if (checkPassword) {
    if (!(eventBody.password && eventBody.password.length >= 7)) {
      return Promise.reject(
        new Error(
          "Password error. Password needs to be longer than 8 characters."
        )
      );
    }
  }

  if (
    !(
      eventBody.lastname &&
      eventBody.lastname.length > 2 &&
      typeof eventBody.lastname === "string"
    )
  ) {
    return Promise.reject(
      new Error("Lastname error. Lastname needs to longer than 2 characters")
    );
  }

  if (
    !(
      eventBody.firstname &&
      eventBody.firstname.length > 2 &&
      typeof eventBody.firstname === "string"
    )
  ) {
    return Promise.reject(
      new Error("Firstname error. Firstname needs to longer than 2 characters")
    );
  }

  if (
    !(
      eventBody.phonenumber &&
      eventBody.phonenumber.length > 9 &&
      typeof eventBody.phonenumber === "string"
    )
  ) {
    return Promise.reject(
      new Error(
        "Phonenumber error. Phonenumber needs to longer than 9 characters"
      )
    );
  }

  if (!(eventBody.email && typeof eventBody.email === "string")) {
    return Promise.reject(
      new Error("Email error. Email must have valid characters.")
    );
  }

  return Promise.resolve();
}

/**
 * Helper for register route
 *
 * @param {*} eventBody
 */
function register(eventBody) {
  return checkIfInputIsValid(eventBody) // validate input
    .then(() =>
      stripe.customers.list(
        { email: eventBody.email } // check if user exists
      )
    )
    .then(
      users =>
        users.data.length > 0
          ? Promise.reject(new Error(USER_ALREADY_EXISTS))
          : bcrypt.hash(eventBody.password, 8) // hash the pass
    )
    .then(hash =>
      // Create the new user
      stripe.customers.create({
        email: eventBody.email,
        metadata: {
          nom: eventBody.lastname,
          prenom: eventBody.firstname,
          numero: eventBody.phonenumber,
          allergy: eventBody.allergy,
          motDePasse: hash
        }
      })
    )
    .then(user => ({ auth: true, token: signToken(user.id) }));
  // sign the token and send it back
}

/**
 * Route to register
 *
 * @param {*} event
 * @param {*} context
 * @param {*} callback
 */
module.exports.register = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  let eventBody = JSON.parse(event.body);

  return register(eventBody)
    .then(session => {
      return mailer
        .createAccount(eventBody.email, eventBody.firstname, eventBody.lastname)
        .then(() =>
          callback(null, {
            statusCode: 200,
            headers: {
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Credentials": true
            },
            body: JSON.stringify(session)
          })
        );
    })
    .catch(err => {
      console.error("USER - Register failed", err, event.body);
      rollbar.error("USER - Register failed", err, event.body);
      rollbar.wait(function() {
        return callback(null, {
          statusCode: err.message === USER_ALREADY_EXISTS ? 409 : 500,
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
            "Content-Type": "text/plain"
          },
          body: err.message
        });
      });
    });
};

/**
 * Helper to login
 *
 * @param {*} eventBody
 */
function login(eventBody) {
  return stripe.customers
    .list(
      { email: eventBody.email } // check if user exists
    )
    .then(users =>
      users.data.length === 0
        ? Promise.reject(new Error(WRONG_CREDENTIALS))
        : comparePassword(
            eventBody.password,
            users.data[0].metadata.motDePasse,
            users.data[0].id
          )
    )
    .then(token => ({ auth: true, token: token }));
}

/**
 * Helper to check if password is correct
 *
 * @param {*} eventPassword
 * @param {*} userPassword
 * @param {*} userId
 */
function comparePassword(eventPassword, userPassword, userId) {
  return bcrypt
    .compare(eventPassword, userPassword)
    .then(passwordIsValid =>
      !passwordIsValid
        ? Promise.reject(new Error(WRONG_CREDENTIALS))
        : signToken(userId)
    );
}

/**
 * Route to login
 *
 * @param {*} event
 * @param {*} context
 * @param {*} callback
 */
module.exports.login = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  return login(JSON.parse(event.body))
    .then(session =>
      callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Credentials": true
        },
        body: JSON.stringify(session)
      })
    )
    .catch(err => {
      console.error("USER - Login failed", err, event.body);
      rollbar.error("USER - Login failed", err, event.body);
      rollbar.wait(function() {
        return callback(null, {
          statusCode: err.message === WRONG_CREDENTIALS ? 400 : 500,
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
            "Content-Type": "text/plain"
          },
          body: err.message
        });
      });
    });
};

/**
 * Helper to get user informations
 *
 * @param {*} userId
 */
function me(userId) {
  return stripe.customers
    .retrieve(userId)
    .then(user => (!user ? Promise.reject("No user found.") : user))
    .catch(err => Promise.reject(new Error(err)));
}

/**
 * Router to get user informations
 *
 * @param {*} event
 * @param {*} context
 * @param {*} callback
 */
module.exports.me = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  return me(event.requestContext.authorizer.principalId) // the decoded.id from the VerifyToken.auth will be passed along as the principalId under the authorizer
    .then(session =>
      callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Credentials": true
        },
        body: JSON.stringify(session)
      })
    )
    .catch(err => {
      console.error("USER - Get informations failed", err, event.body);
      rollbar.error("USER - Get informations failed", err, event.body);
      rollbar.wait(function() {
        return callback(null, {
          statusCode: err.statusCode || 500,
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
            "Content-Type": "text/plain"
          },
          body: err.message
        });
      });
    });
};

/**
 * Helper to update user informations
 *
 * @param {*} eventBody
 * @param {*} userId
 */
function update(eventBody, userId) {
  return checkIfInputIsValid(eventBody, false) // validate input
    .then(() => stripe.customers.retrieve(userId))
    .then(user =>
      !user
        ? Promise.reject(new Error("User with that email exists."))
        : typeof eventBody.password !== "undefined"
        ? bcrypt.hash(eventBody.password, 8)
        : user.metadata.motDePasse
    )
    .then(
      hash =>
        // Create the new user
        stripe.customers.update(userId, {
          email: eventBody.email,
          metadata: {
            nom: eventBody.lastname,
            prenom: eventBody.firstname,
            numero: eventBody.phonumber,
            allergy: eventBody.allergy,
            motDePasse: hash
          }
        })
      // update the existing user
    )
    .then(user => ({ auth: true, token: signToken(user.id) }));
  // sign the token and send it back
}

/**
 * Route to update user informations
 *
 * @param {*} event
 * @param {*} context
 * @param {*} callback
 */
module.exports.update = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  return update(
    JSON.parse(event.body),
    event.requestContext.authorizer.principalId
  )
    .then(session =>
      callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Credentials": true
        },
        body: JSON.stringify(session)
      })
    )
    .catch(err => {
      console.error("USER - Update failed", err, event.body);
      rollbar.error("USER - Update failed", err, event.body);
      rollbar.wait(function() {
        return callback(null, {
          statusCode: err.statusCode || 500,
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
            "Content-Type": "text/plain"
          },
          body: err.message
        });
      });
    });
};

/**
 * Check if input params to reset password are valid
 *
 * @param {*} eventBody
 */
function checkIfResetPasswordParamsValid(eventBody) {
  if (!eventBody.email) {
    return Promise.reject(new Error("Missing params to reset password."));
  }

  return Promise.resolve();
}

/**
 * Reset password for a customer
 *
 * @param {*} email
 */
function resetPassword(email) {
  let newPassword = generateNewPassword();
  let encryptNewPassword = "";

  return bcrypt
    .hash(newPassword, 8)
    .then(hash => {
      encryptNewPassword = hash;

      return stripe.customers.list(
        { email: email } // check if user exists
      );
    })
    .then(user =>
      user.data.length > 0
        ? stripe.customers.update(user.data[0].id, {
            metadata: {
              motDePasse: encryptNewPassword
            }
          })
        : Promise.reject(new Error(USER_DOESNT_EXIST))
    )
    .then(() => {
      return mailer.resetPassword(email, newPassword);
    });
}

/**
 * Generate a securised random password
 */
function generateNewPassword() {
  return Math.random()
    .toString(36)
    .slice(-8);
}

/**
 * Route to reset password
 *
 * @param {*} event
 * @param {*} context
 * @param {*} callback
 */
module.exports.reset = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  let eventBody = JSON.parse(event.body);

  return checkIfResetPasswordParamsValid(eventBody)
    .then(() => resetPassword(eventBody.email))
    .then(() =>
      callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Credentials": true
        },
        body: JSON.stringify(PASSWORD_RESET)
      })
    )
    .catch(err => {
      console.error("USER - Reset password failed", err, event.body);
      rollbar.error("USER - Reset password failed", err, event.body);
      rollbar.wait(function() {
        return callback(null, {
          statusCode: err.message === USER_DOESNT_EXIST ? 404 : 500,
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
            "Content-Type": "text/plain"
          },
          body: err.message
        });
      });
    });
};
