const jwt = require("jsonwebtoken"); // used to create, sign, and verify tokens
const Rollbar = require("rollbar");
const rollbar = new Rollbar({
  accessToken: process.env.ROLLBAR_TOKEN,
  environment: process.env.ROLLBAR_ENVIRONMENT,
  captureUncaught: true,
  captureUnhandledRejections: true
});

// Policy helper function
const generatePolicy = (principalId, effect, resource) => {
  const authResponse = {};
  authResponse.principalId = principalId;
  if (effect && resource) {
    const policyDocument = {};
    policyDocument.Version = "2012-10-17";
    policyDocument.Statement = [];
    const statementOne = {};
    statementOne.Action = "execute-api:Invoke";
    statementOne.Effect = effect;
    statementOne.Resource = resource;
    policyDocument.Statement[0] = statementOne;
    authResponse.policyDocument = policyDocument;
  }
  return authResponse;
};

module.exports.auth = rollbar.lambdaHandler((event, context, callback) => {
  // check header or url parameters or post parameters for token
  const token = event.authorizationToken;

  if (!token) return callback("Unauthorized");

  // verifies secret and checks exp
  jwt.verify(
    token,
    process.env.JWT_SECRET,
    { ignoreExpiration: true },
    (err, decoded) => {
      if (err) {
        console.error("TOKEN - Verify failed", err, event.authorizationToken);
        return callback("Unauthorized");
      }

      // if everything is good, save to request for use in other routes
      return callback(
        null,
        generatePolicy(decoded.id, "Allow", event.methodArn)
      );
    }
  );
});
