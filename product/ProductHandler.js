const jwt = require("jsonwebtoken");
const bcrypt = require("bcryptjs-then");
const stripe = require("stripe")(process.env.STRIPE_KEY);
const Rollbar = require("rollbar");
const rollbar = new Rollbar({
  accessToken: process.env.ROLLBAR_TOKEN,
  environment: process.env.ROLLBAR_ENVIRONMENT,
  captureUncaught: true,
  captureUnhandledRejections: true
});

/**
 * Check if input is valid for add and update product name
 *
 * @param {*} eventBody
 */
function checkIfInputIsValid(eventBody) {
  if (!eventBody.id) {
    return Promise.reject(new Error("Product ID is required"));
  }

  if (!eventBody.name) {
    return Promise.reject(new Error("Product name is required"));
  }

  if (!eventBody.price) {
    return Promise.reject(new Error("Price is required"));
  }

  return Promise.resolve();
}

/**
 * Helper for add product route
 *
 * @param {*} eventBody
 */
function add(eventBody) {
  // Create the new product in Stripe
  return stripe.products
    .create({
      id: eventBody.id,
      name: eventBody.name,
      type: "good",
      shippable: false
    })
    .then(product =>
      stripe.skus.create({
        product: product.id,
        currency: "EUR",
        inventory: {
          type: "bucket",
          value: "in_stock"
        },
        price: Math.round(eventBody.price * 100)
      })
    );
}

/**
 * Helper to update product name in Stripe
 *
 * @param {*} eventBody
 * @param {*} productId
 */
function update(eventBody) {
  return stripe.products
    .retrieve(eventBody.id)
    .then(
      product =>
        stripe.products.update(product.id, {
          name: eventBody.name
        })
      // update the existing product
    )
    .then(product =>
      stripe.skus.list(
        { product: product.id } // check if product exists
      )
    )
    .then(sku =>
      sku.data.length > 0
        ? stripe.skus.update(sku.data[0].id, {
            price: Math.round(eventBody.price * 100)
          })
        : stripe.skus.create({
            product: eventBody.id,
            currency: "EUR",
            inventory: {
              type: "bucket",
              value: "in_stock"
            },
            price: Math.round(eventBody.price * 100)
          })
    );
}

/**
 * Route to update user informations
 *
 * @param {*} event
 * @param {*} context
 * @param {*} callback
 */
module.exports.update = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  let eventBody = JSON.parse(event.body);

  return checkIfInputIsValid(eventBody) // validate input
    .then(() =>
      stripe.products.list(
        { ids: [eventBody.id] } // check if product exists
      )
    )
    .then(response =>
      response.data.length > 0
        ? update(JSON.parse(event.body))
        : add(JSON.parse(event.body))
    )
    .then(product =>
      callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Credentials": true
        },
        body: JSON.stringify(product)
      })
    )
    .catch(err => {
      console.error("PRODUCT - Update failed", err, event.body);
      rollbar.error("PRODUCT - Update failed", err, event.body);
      rollbar.wait(function() {
        return callback(null, {
          statusCode: err.statusCode || 500,
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
            "Content-Type": "text/plain"
          },
          body: err.message
        });
      });
    });
};
