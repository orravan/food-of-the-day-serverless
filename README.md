![Food of the day](https://i.screenshot.net/po2gvul)

# Food Of The Day

Food Of the Day (FOD) is a native application developed in React Native for a French company in Montpellier. It's a e-shop food application who gives the possibility to see meals purposed by the company and order it. The customer pays directly in the app, chooses a delivery point (e.g. a company or a gym based on Montpellier), he can see his order status and receive a notification (+ email) when the order is paid and delivered.

## Getting started

FOD uses tools below to work :

![Contentful](https://i.screenshot.net/wp4jqh6)
![Stripe](https://i.screenshot.net/3k86mbn)
![OneSignal](https://i.screenshot.net/ndxw7sk)
![Rollbar](https://i.screenshot.net/mx4dqco)
![Mailjet](https://i.screenshot.net/7yo3dcw)
![Serverless](https://i.screenshot.net/7yozqcz)

- [Contentful](https://www.contentful.com/) : CMS used in this project to manage meals, delivery points and meal types availables on application. This is a extract of Content model used in Contentful : [contentful-export.json](https://srv-file5.gofile.io/download/Ivo3oS/contentful-export.json)
- [Stripe](https://stripe.com/) : Online payment solution to manage customers informations, orders and payments.
- [OneSignal](https://onesignal.com/) : Push notifications tools used to send native notification when an order is delivered to a delivery point or if an order is refund for example.
- [Rollbar](https://rollbar.com/) : Error tracking and debugging tools to centralize errors on application.
- [Mailjet](https://fr.mailjet.com/) : Emailing solution to send transactional or campaign emails. In this project we use it mainly to send email in theses use cases : account creation, reset password, order confirmation, order delivery or order refund.
- [Serverless](https://serverless.com/) API project : [Food of the day servelerss](https://gitlab.com/orravan/food-of-the-day-serverless) securised Node JS API (JWT) which centralizes calls between Stripe, OneSignal and Mailjet.

## Available Scripts

1. **Install serverless CLI :**

```bash
npm install -g serverless
```

2. **Install dependencies :**

```bash
npm i
```

3. **Copy secrets.json.dist to secrets.json and fill it with required informations :**

4. **Run on localhost to test :**

```bash
sls offline
```

5. **Configure** [AWS credentials](https://github.com/serverless/serverless/blob/master/docs/providers/aws/guide/credentials.md) :

6. **Deploy :**

```bash
sls deploy -y
```

## Contributing

Interested in contributing to this repo? Just follow [Git flow](https://datasift.github.io/gitflow/IntroducingGitFlow.html) and submit a PR for a new feature/hotfix.

## Licensing

The code in this project is licensed under MIT license.
