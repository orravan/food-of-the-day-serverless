const stripe = require("stripe")(process.env.STRIPE_KEY);
const mailer = require("../mailer/Mailer");
const Rollbar = require("rollbar");
const rollbar = new Rollbar({
  accessToken: process.env.ROLLBAR_TOKEN,
  environment: process.env.ROLLBAR_ENVIRONMENT,
  captureUncaught: true,
  captureUnhandledRejections: true
});
const OneSignal = require("onesignal-node");
const myClient = new OneSignal.Client({
  userAuthKey: process.env.ONE_SIGNAL_USER_AUTH_KEY,
  // note that "app" must have "appAuthKey" and "appId" keys
  app: {
    appAuthKey: process.env.ONE_SIGNAL_APP_AUTH_KEY,
    appId: process.env.ONE_SIGNAL_APP_ID
  }
});

/**
 * Check if input is valid to create an order
 *
 * @param {*} eventBody
 */
function checkIfInputIsValid(eventBody) {
  if (
    !eventBody.items ||
    !Array.isArray(eventBody.items) ||
    !eventBody.metadata ||
    !eventBody.sourceId
  ) {
    return Promise.reject(
      new Error("Missing informations to create an order.")
    );
  }

  eventBody.items.forEach(function(item) {
    if (!item.id || !item.quantity) {
      return Promise.reject(new Error("Missing informations about items."));
    }
  });

  return Promise.resolve();
}

/**
 * Format items to create order
 *
 * @param {*} items
 */
function formatItems(items) {
  var formattedItems = [];

  items.forEach(function(item) {
    formattedItems.push({
      type: "sku",
      parent: item.id,
      quantity: item.quantity,
      description: item.allergy
        ? "Allergie : " + item.allergy
        : "Pas d'allergie"
    });
  });

  return Promise.resolve(formattedItems);
}

function replaceProductBySku(items) {
  let promises = items.map(function(item) {
    return stripe.skus
      .list(
        { product: item.id } // check if product exists
      )
      .then(function(sku) {
        item.id = sku.data.length > 0 ? sku.data[0].id : item.id;

        return item;
      });
  });

  return Promise.all(promises);
}

/**
 * Route to create and payd an order
 *
 * @param {*} event
 * @param {*} context
 * @param {*} callback
 */
module.exports.create = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  let eventBody = JSON.parse(event.body);

  return checkIfInputIsValid(eventBody) // validate input
    .then(() => replaceProductBySku(eventBody.items))
    .then(items => formatItems(items))
    .then(items =>
      typeof eventBody.coupon !== "undefined" && eventBody.coupon
        ? stripe.orders.create({
            currency: "EUR",
            items: items,
            coupon: eventBody.coupon,
            customer: event.requestContext.authorizer.principalId,
            metadata: eventBody.metadata
          })
        : stripe.orders.create({
            currency: "EUR",
            items: items,
            customer: event.requestContext.authorizer.principalId,
            metadata: eventBody.metadata
          })
    )
    .then(order =>
      stripe.orders.pay(order.id, {
        customer: event.requestContext.authorizer.principalId,
        source: eventBody.sourceId
      })
    )
    .then(orderPayed => {
      return mailer
        .createOrder(
          orderPayed.email,
          orderPayed.id,
          orderPayed.metadata.dateLivraison,
          orderPayed.metadata.lieuLivraison,
          orderPayed.items,
          orderPayed.amount
        )
        .then(() =>
          callback(null, {
            statusCode: 200,
            headers: {
              "Access-Control-Allow-Origin": "*",
              "Access-Control-Allow-Credentials": true
            },
            body: JSON.stringify(orderPayed)
          })
        );
    })
    .catch(err => {
      console.error("ORDER - Creation failed", err, event.body);
      rollbar.error("ORDER - Creation failed", err, event.body);
      rollbar.wait(function() {
        return callback(null, {
          statusCode: err.statusCode || 500,
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
            "Content-Type": "text/plain"
          },
          body: err.message
        });
      });
    });
};

/**
 * Helper to list customer orders
 *
 * @param {*} userId
 */
function list(userId) {
  return stripe.orders
    .list({ customer: userId })
    .then(orders => (!orders ? Promise.reject("No orders found.") : orders))
    .catch(err => Promise.reject(new Error(err)));
}

/**
 * Route to list orders for a customer ID given
 *
 * @param {*} event
 * @param {*} context
 * @param {*} callback
 */
module.exports.list = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  return list(event.requestContext.authorizer.principalId)
    .then(orders =>
      callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Credentials": true
        },
        body: JSON.stringify(orders)
      })
    )
    .catch(err => {
      console.error("ORDER - List failed", err, event.body);
      rollbar.error("ORDER - List failed", err, event.body);
      rollbar.wait(function() {
        return callback(null, {
          statusCode: err.statusCode || 500,
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
            "Content-Type": "text/plain"
          },
          body: err.message
        });
      });
    });
};

/**
 * Check if input is valid to send email when an
 * order is updated
 *
 * @param {*} eventBody
 */
function checkIfOrderIsValid(eventBody) {
  if (
    !eventBody.data.object.items ||
    !Array.isArray(eventBody.data.object.items) ||
    !eventBody.data.object.metadata ||
    !eventBody.data.object.email ||
    !eventBody.data.object.status
  ) {
    return Promise.reject(
      new Error("Missing informations to send email of an order.")
    );
  }

  return Promise.resolve();
}

/**
 * Route to update orders status : e.g. send an email when
 * the order is shipped or refund
 *
 * @param {*} event
 * @param {*} context
 * @param {*} callback
 */
module.exports.update = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;

  let eventBody = JSON.parse(event.body);
  return checkIfOrderIsValid(eventBody)
    .then(() => {
      if (eventBody.data.object.status === "fulfilled") {
        let fulfilledNotification = new OneSignal.Notification({
          contents: {
            en:
              "Votre commande est livrée à " +
              eventBody.data.object.metadata.lieuLivraison,
            fr:
              "Votre commande est livrée à " +
              eventBody.data.object.metadata.lieuLivraison
          },
          include_external_user_ids: [eventBody.data.object.customer]
        });

        myClient.sendNotification(fulfilledNotification, function(
          err,
          httpResponse,
          data
        ) {
          if (err) {
            console.error("ORDER - Notification failed", err, event.body);
            rollbar.error("ORDER - Notification failed", err, event.body);
          }
        });

        return mailer.orderShipped(
          eventBody.data.object.email,
          eventBody.data.object.id,
          eventBody.data.object.metadata.dateLivraison,
          eventBody.data.object.metadata.lieuLivraison,
          eventBody.data.object.items,
          eventBody.data.object.amount
        );
      } else if (eventBody.data.object.status === "canceled") {
        let cancelNotification = new OneSignal.Notification({
          contents: {
            en: "Annulation de votre commande n°" + eventBody.data.object.id,
            fr: "Annulation de votre commande n°" + eventBody.data.object.id
          },
          include_external_user_ids: [eventBody.data.object.customer]
        });

        myClient.sendNotification(cancelNotification, function(
          err,
          httpResponse,
          data
        ) {
          if (err) {
            console.error("ORDER - Notification failed", err, event.body);
            rollbar.error("ORDER - Notification failed", err, event.body);
          }
        });

        return mailer.orderCanceled(
          eventBody.data.object.email,
          eventBody.data.object.id,
          eventBody.data.object.metadata.dateLivraison,
          eventBody.data.object.metadata.lieuLivraison,
          eventBody.data.object.items,
          eventBody.data.object.amount
        );
      } else {
        return Promise.resolve();
      }
    })
    .then(() =>
      callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Credentials": true
        },
        body: "Email send"
      })
    )
    .catch(err => {
      console.error("ORDER - Update failed", err, event.body);
      rollbar.error("ORDER - Update failed", err, event.body);
      rollbar.wait(function() {
        return callback(null, {
          statusCode: err.statusCode || 500,
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
            "Content-Type": "text/plain"
          },
          body: err.message
        });
      });
    });
};
