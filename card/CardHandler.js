const stripe = require("stripe")(process.env.STRIPE_KEY);
const Rollbar = require("rollbar");
const rollbar = new Rollbar({
  accessToken: process.env.ROLLBAR_TOKEN,
  environment: process.env.ROLLBAR_ENVIRONMENT,
  captureUncaught: true,
  captureUnhandledRejections: true
});

/**
 * Check if input is valid to add a credit card to a customer
 *
 * @param {*} eventBody
 */
function checkIfInputIsValid(eventBody) {
  if (
    !eventBody.name ||
    !eventBody.number ||
    !eventBody.expMonth ||
    !eventBody.expYear ||
    !eventBody.cvc
  ) {
    return Promise.reject(
      new Error("Missing informations to add credit card.")
    );
  }

  return Promise.resolve();
}

/**
 * Check if input is valid to delete credit card
 *
 * @param {*} eventBody
 */
function inputIsValidToDeleteCard(eventBody) {
  if (!eventBody.cardId) {
    return Promise.reject(
      new Error("Missing informations to delete credit card.")
    );
  }

  return Promise.resolve();
}
/**
 * Route to create a credit card and remove the old one
 *
 * @param {*} event
 * @param {*} context
 * @param {*} callback
 */
module.exports.create = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  let eventBody = JSON.parse(event.body);

  return checkIfInputIsValid(eventBody) // validate input
    .then(() =>
      typeof eventBody.cardId !== "undefined" && eventBody.cardId
        ? stripe.customers.deleteSource(
            event.requestContext.authorizer.principalId,
            eventBody.cardId
          )
        : Promise.resolve()
    )
    .then(() =>
      stripe.sources.create({
        type: "card",
        currency: "EUR",
        card: {
          number: eventBody.number,
          exp_month: eventBody.expMonth,
          exp_year: eventBody.expYear,
          cvc: eventBody.cvc
        },
        owner: {
          name: eventBody.name
        }
      })
    )
    .then(source =>
      stripe.customers.createSource(
        event.requestContext.authorizer.principalId,
        {
          source: source.id
        }
      )
    )
    .then(card =>
      callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Credentials": true
        },
        body: JSON.stringify(card)
      })
    )
    .catch(err => {
      console.error("CARD - Creation failed", err, event.body);
      rollbar.error("CARD - Creation failed", err, event.body);
      rollbar.wait(function() {
        return callback(null, {
          statusCode: err.statusCode || 500,
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
            "Content-Type": "text/plain"
          },
          body: err.message
        });
      });
    });
};

/**
 * Route to delete a credit card
 *
 * @param {*} event
 * @param {*} context
 * @param {*} callback
 */
module.exports.delete = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  let eventBody = JSON.parse(event.body);

  return inputIsValidToDeleteCard(eventBody) // validate input
    .then(() =>
      stripe.customers.deleteSource(
        event.requestContext.authorizer.principalId,
        eventBody.cardId
      )
    )
    .then(deletedCard =>
      callback(null, {
        statusCode: 200,
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Credentials": true
        },
        body: JSON.stringify(deletedCard)
      })
    )
    .catch(err => {
      console.error("CARD - Delete failed", err, event.body);
      rollbar.error("CARD - Delete failed", err, event.body);
      rollbar.wait(function() {
        return callback(null, {
          statusCode: err.statusCode || 500,
          headers: {
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Credentials": true,
            "Content-Type": "text/plain"
          },
          body: err.message
        });
      });
    });
};
